angular.module('huaguoApp')
	.factory('huaguoService',['Restangular',function(Restangular){
      var newsAngular=Restangular.withConfig(function(Configurer){

      })
      var newsService=newsAngular.all('/news');
      return {
          getNewsList: function () {
              return newsService.customGET();
          },
          getNewsInfo:function(id){
              return newsService.customGET(id);
          }
      }
	}])
	.factory('userService',['Restangular',function(Restangular){
      var userAngular=Restangular.withConfig(function(Configurer){
        //服务个性化配置
      });
      var loginService=userAngular.all('/users/login');
      var regService=userAngular.all('/users/register');
      return{
          login:function(user){
            return loginService.customPOST(user);
          },
          register:function(userInfo){
            return regService.customPOST(userInfo);
          }
      }
	}])
  .factory('topicService',['Restangular','$rootScope',function(Restangular){
      var topicAngular=Restangular.withConfig(function(Configurer){
          //服务个性化配置

      });
      var topicService=topicAngular.all('/topics');
      var addTopicService=topicAngular.all('/topics/addTopic');
      return{
          getTopicsList: function (pageIndex) {
              return topicService.customGET('all',{currentPage:pageIndex});
          },
          getTopicsType: function (type,pageIndex) {
              return topicService.customGET(type,{currentPage:pageIndex});
          },
          addTopic:function(topic,token){
              return addTopicService.customPOST(topic,'',{},{authorization:token});
          },
          showTopic:function(id){
              return topicService.customGET('topic/'+id);
          }

      }
  }])
