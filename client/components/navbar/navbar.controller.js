'use strict';

angular.module('huaguoApp')
  .controller('header', function ($rootScope, $scope, $location, $localStorage) {
    $scope.menu = [{
      'title': 'Home',
      'link': '/'
    }];

    $scope.isCollapsed = true;

    $scope.logout = function() {
      delete $localStorage.user;
      $rootScope.plugin=false;
      $location.path('/login');
    };

    $scope.isActive = function(route) {
      var temp=$location.path().split('/')[1];
      return route === temp;
    };
  });
