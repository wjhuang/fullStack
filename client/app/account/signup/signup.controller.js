'use strict';

angular.module('huaguoApp')
    .controller('SignupCtrl', function ($rootScope,$scope,$state,userService,$localStorage,uuid4,Upload) {
        $scope.user = {};
        $scope.userFormToltip=true;
        var uuid=uuid4.generate();      //为用户生成avator的名称

        $scope.register = function() {
            var temp=$scope.user;
            delete temp.repeatpassword;
            temp.avator = './assets/avators/'+uuid+'.png';
            userService.register(temp).then(function(res){
                $rootScope.plugin=true;
                $rootScope.avator=res.data.avator;
                $rootScope.username=res.data.username;
                $localStorage.user = res.data;
                $state.go('main');
            });
            var str= $scope.file.name.slice(-4);
            Upload.rename($scope.file, uuid + str);
            $scope.upload($scope.file);
        }

        $scope.upload = function (file) {
            Upload.upload({
                url: '/api/avator/upload',
                data: {file: file}
            }).then(function (res) {
                console.log(res);
            }, function (res) {
                console.log('Error status: ' + res.status);
            },function(evt){
                console.log(evt);
            })
        };
  })
