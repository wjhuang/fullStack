'use strict';

angular.module('huaguoApp')
    .controller('LoginCtrl', function($rootScope,$scope, userService, $localStorage, $state) {
        $scope.user = {};
        $scope.errors = {};
        $scope.userFormToltip = true; //用户登录提示信息默认隐藏
        $scope.captchaToltip = true; //验证码提示信息默认隐藏

        $scope.changeCcap = function() { //点击切换验证码
            var date = new Date();
            var stamp = date.getTime();
            $scope.captcha = "/img?aa" + stamp;
        }

        $scope.login = function() {
            var user = {
                email: $scope.user.email,
                password: $scope.user.password,
                captcha: $scope.user.captcha
            }
            userService.login(user).then(function(res) {
                if (res.code == 0) {
                    $scope.captchaToltip = false;
                } else if (res.code == 400 || res.code == 404) {
                    $scope.userFormToltip = false;
                } else {
                    $rootScope.plugin=true;
                    $rootScope.avator=res.data.avator;
                    $rootScope.username=res.data.username;
                    $localStorage.user = res.data;
                    $state.go('main');
                }
            })
        };

    });
