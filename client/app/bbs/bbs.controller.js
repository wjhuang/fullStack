/**
 * Created by admin on 2016/1/13.
 */
angular.module('huaguoApp')
    .controller('bbsCtrl',function($scope,$localStorage,$state,$window,topicService){
        $scope.currentPage = 1;
        $scope.topiceType="all";
        topicService.getTopicsList($scope.currentPage).then(function(res){   //默认加载所有类型的话题
            if(res.code=200){
                $scope.topics=res.data;
                angular.forEach($scope.topics,function(item){
                    if(item.topicTypeEn =="lististop"){
                        $scope.topics.pop(item);
                        $scope.topics.unshift(item);
                    }
                })
                $scope.itemsTotal=res.itemTotal;
            }
        });
        $scope.selectAll=function(){            //获取所有话题贴
            $scope.topiceType="all";
            topicService.getTopicsList($scope.currentPage).then(function(res){
                if(res.code=200){
                  $scope.topics=res.data;
                  angular.forEach($scope.topics,function(item){
                    if(item.topicTypeEn =="lististop"){
                      $scope.topics.pop(item);
                      $scope.topics.unshift(item);
                    }
                  })
                }
            });
        };
        $scope.selectCream=function(){          //获取所有精华贴
            $scope.topiceType="cream";
            topicService.getTopicsType('cream',$scope.currentPage).then(function(res){
                if(res.code=200){
                    $scope.topics=res.data;
                    $scope.itemsTotal=res.itemTotal;
                }
            });
        };
        $scope.selectShare=function(){          //获取所有分享贴
            $scope.topiceType="share";
            topicService.getTopicsType('share',$scope.currentPage).then(function(res){
                if(res.code=200){
                    $scope.topics=res.data;
                    $scope.itemsTotal=res.itemTotal;
                }
            });
        };
        $scope.pageChanged=function(){
            if($scope.topiceType=="all"){
                topicService.getTopicsList($scope.currentPage).then(function(res){
                    if(res.code=200){
                        $scope.topics=res.data;
                        $scope.itemsTotal=res.itemTotal;
                    }
                });
            }else if($scope.topiceType=="cream"){
                topicService.getTopicsType('cream',$scope.currentPage).then(function(res){
                    if(res.code=200){
                        $scope.topics=res.data;
                        $scope.itemsTotal=res.itemTotal;
                    }
                });
            }else{
                topicService.getTopicsType('share',$scope.currentPage).then(function(res){
                    if(res.code=200){
                        $scope.topics=res.data;
                        $scope.itemsTotal=res.itemTotal;
                    }
                });
            }

        }
        $scope.addTopic=function(){
            if($localStorage.user){
                $state.go('bbs.addTopic');
            }else{
                $window.alert('你未拥有权限，请登录！');
            }
        }
    })
    .controller('addTopicCtrl',function($scope,$localStorage,topicService){
        $scope.topic={};
        $scope.submit=function(){
            $scope.topic.authorName=$localStorage.user.username;
            $scope.topic.authorAvator=$localStorage.user.avator;
            /*$scope.topic.topicTypeEn='share';
            $scope.topic.topicType='分享';*/
            $scope.topic.createTime=moment().format("YYYY-MM-DD HH:mm");
            topicService.addTopic($scope.topic,$localStorage.user.token).then(function(res){
                if(res.code==200){
                    console.log(res);
                }
            })
        }
    })
    .controller('topicCtrl',function($scope,$stateParams,topicService){
        var topicId=$stateParams.topicId;
        topicService.showTopic(topicId).then(function(res){
            if(res.code==200){
                $scope.topic=res.data;
                if(res.authorInfo.sign == ""){
                    res.authorInfo.sign="这家伙很懒，什么都没留下";
                }
                $scope.authorInfo=res.authorInfo;
            }
        })
    })
