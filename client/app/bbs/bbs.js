/**
 * Created by admin on 2016/1/13.
 */
angular.module('huaguoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('bbs',{
        url:'/bbs',
        abstract:true,
        templateUrl:'app/bbs/index.html',
      })
      .state('bbs.index',{
        url:'/index',
        templateUrl:'app/bbs/topicList.html',
        controller:'bbsCtrl'
      })
      .state('bbs.addTopic',{
        url:'/addTopic',
        templateUrl:'app/bbs/addTopic.html',
        controller:'addTopicCtrl'
      })
      .state('bbs.topic',{
        url:'/topic/:topicId',
        templateUrl:'app/bbs/showTopic.html',
        controller:'topicCtrl'
      })
  })
