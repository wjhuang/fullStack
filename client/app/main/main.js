'use strict';

angular.module('huaguoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/home',
        templateUrl: 'app/main/main.html',
        controller: 'MainCtrl'
      })
      .state('notice',{
        url:'/notice/:noticeId',
        templateUrl:'app/main/notice.html',
        controller:'NoticeCtrl'
      })
  });

