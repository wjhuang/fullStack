'use strict';

angular.module('huaguoApp')
    .controller('MainCtrl', function($scope, $http, socket, huaguoService,$state) {
        $scope.lastTime=moment().format('YYYY-MM-DD');
        $scope.refund="按月付息到期还本";
        huaguoService.getNewsList().then(function(res){
            $scope.newsList=res;
        })

        $scope.login=function(){
            $state.go('login');
        }
        $scope.register=function(){
            $state.go('signup');
        }
    })
    .directive('slider', function() {
        return {
            restrict: 'AE',
            replace: true,
            template: '<div id="slider">' +
                '<img src="../../assets/images/banner1.jpg" data-src-2x="../../assets/images/banner1.jpg" alt="Slide 1" />' +
                '<img data-src="../../assets/images/banner2.jpg" data-src-2x="../../assets/images/banner2.jpg" src="" alt="Slide 2" />' +
                '<img data-src="../../assets/images/banner3.jpg" data-src-2x="../../assets/images/banner3.jpg" src="" alt="Slide 3" />' +
                '<img data-src="../../assets/images/banner4.jpg" data-src-2x="../../assets/images/banner4.jpg" src="" alt="Slide 4" /></div>',
            link:function(scope,element,attr){
                var slider = new IdealImageSlider.Slider({
                    selector: "#slider",
                    height: 400, // Required but can be set by CSS
                    interval: 4000
                });
                slider.start();
            }
        }
    } )
    .controller('NoticeCtrl',function($scope,$stateParams,huaguoService){
        var noticeId= $stateParams.noticeId;
        huaguoService.getNewsInfo(noticeId).then(function(res){
            var str=res.data.content;
            console.log(str.replace("\r", ""))
            $scope.news=res.data;
            console.log(JSON.stringify($scope.news));
        });
    });
