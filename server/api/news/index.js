'use strict';
var express=require('express');
var controller=require('./news.controller');

var router=express.Router();
router.get('/',controller.index);
router.get('/:id',controller.search);

module.exports=router;
