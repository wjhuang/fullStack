var newsModel=require('./news.model');

// Get list of news
exports.index = function(req, res) {
  newsModel.find(function (err, news) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(news);
  });
};

//查询公告的详细信息
exports.search=function(req,res){
  var noticeId=req.params.id;
  newsModel.findOne({_id:noticeId},function(err,news){
      if(news){
          res.send({code:200,data:news});
      }else{
          res.send({code:400,msg:'查询有误'});
      }
  })
}

