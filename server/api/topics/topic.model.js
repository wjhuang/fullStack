/**
 * Created by admin on 2016/1/13.
 */
var mongoose = require ( 'mongoose' );

var topicSchema = new mongoose.Schema ( {
  title : { type : String } ,
  content : { type : String } ,
  topicType : { type : String , default : '交流' } ,
  topicTypeEn : { type : String , default : 'common' } ,
  authorId : { type : String } ,
  authorName : { type : String } ,
  authorAvator : { type : String } ,
  createTime : { type : String } ,
  up:{ type : Number, default:0 } ,
  low:{ type : Number, default:0 } ,
  comments : {
      type : [ {
          content : { type : String } ,
          commenterId : { type : String } ,
          commenterName : { type : String } ,
          commenterAvator : { type : String } ,
          commentTime : { type : String }
      } ],
      default:[]
  }
})

module.exports = mongoose.model ( 'topic' , topicSchema );
