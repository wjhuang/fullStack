/**
 * Created by admin on 2016/1/13.
 */
'use strict';

var express = require('express');
var controller = require('./topic.controller');

var router = express.Router();

router.get('/all',controller.index);
router.get('/:topicType',controller.topicType);
router.post('/addTopic',controller.addTopic);
router.get('/topic/:topicId',controller.showTopic);

module.exports = router;
