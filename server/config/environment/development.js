'use strict';

// Development specific configuration
// ==================================
module.exports = {
  // MongoDB connection options
  useMongo:false,
  mongo: {
    uri: 'mongodb://localhost/huaguo-dev'
  },

  seedDB: true
};
