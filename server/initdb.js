/* 使用爬虫从原网站获取数据信息*/
var eventproxy = require('eventproxy');
var superagent = require('superagent');
var cheerio = require('cheerio');
var url = require('url');
var newsModel=require('./api/news/news.model');
var userModel=require('./api/user/user.model');

var mongoose = require('mongoose');
var config = require('./config/environment');

// Connect to database
if(!config.useMongo){
	mongoose.connect(config.mongo.uri, config.mongo.options);
	mongoose.connection.on('error', function(err) {
		console.error('MongoDB connection error: ' + err);
		process.exit(-1);
		}
	);
	// Populate DB with sample data
	if(config.seedDB) { require('./config/seed'); }
}

var ep = new eventproxy();
var newsList=[];
var codeUrl = 'http://www.huaguo.cn/';
console.log('创建数据中，请耐心等待...');
superagent.get('http://www.huaguo.cn/')
    .end(function(err, res) {
        if (err) {
            return console.error(err);
        }
        var topicUrls = [];
        var items = [];
        var $ = cheerio.load(res.text);
        $('.index_news_list').eq(1).find('ul li').each(function(idx, element) {
            var $element = $(element);
            var href = url.resolve(codeUrl, $element.find('a').attr('href'));
            topicUrls.push(href);
        });
        topicUrls.forEach(function(topicUrl) {
            superagent.get(topicUrl)
                .end(function(err, res) {
                    if (err) {
                        return console.error(err);
                    }
                    var $ = cheerio.load(res.text);
                    var news = {
                        title: $('.gonggao_con .title').text().trim(),
                        date: $('.gonggao_con .note div').eq(1).text().trim(),
                        content: $('.gonggao_con .note').text(),
                    }
                    ep.emit('topic_content', news);
                })
        })
        ep.after('topic_content', topicUrls.length, function(topics) {
        	console.log('爬虫完毕!');
            ep.emit('completed',topics);
        });
    });
ep.on('completed',function(data){
	data.forEach(function(item,index){
		newsModel.create(item, function (err,data) {
	        if (err){
	            throw err;
	        }else{
	            console.log('数据'+index+'创建完毕！')
	        }
	    })
	})
    userModel.create({
        email:'admin@163.com',
        username:'admin',
        password:'admin',
        avator:'./asset/avators/default.png',
        phone:'15007146052',
        token:''
    },function(err,data){
        if (err){
            throw err;
        }else{
            console.log('管理员创建成功');
        }
    })
})
