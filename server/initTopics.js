/**
 * Created by admin on 2016/1/13.
 */
var topicModel = require ( './api/topics/topic.model' );
var userModel=require ( './api/user/user.model' );
var mongoose = require ( 'mongoose' );
var config   = require ( './config/environment' );

// Connect to database
if ( ! config.useMongo ) {
  mongoose.connect ( config.mongo.uri , config.mongo.options );
  mongoose.connection.on ( 'error' , function ( err ) {
      console.error ( 'MongoDB connection error: ' + err );
      process.exit ( - 1 );
    }
  );
  // Populate DB with sample data
  if ( config.seedDB ) {
    require ( './config/seed' );
  }
}
var topicList = [ {
  'title' : '通过阿里大鱼平台用nodejs发短信' ,
  'topicType' : '置顶' ,
  'topicTypeEn' : 'lististop' ,
  'authorId' : '56933f762125dff800ed0f0c' ,
  'authorName' : 'wjhuang' ,
  'authorAvator' : './assets/avators/default.png' ,
  'createTime' : '2016-01-12 13:29' ,
  'content' : '通过nodejs发短信，想想就很激动，这样我就可以给网站加个会员注册短信验证功能、会员登录短信验证功能、网站异常自定义发短信功能。下面我将介绍如何通过阿里大鱼提供的api接                     口完成这些功能。' ,
  'comments' : [
    {
      'content' : '很棒，已mark' ,
      'commenterId' : '56933f762125dff800ed0f0c' ,
      'commenterName' : 'wjhuang' ,
      'commenterAvator' : './assets/avators/default.png' ,
      'createTime' : '2016-01-13 15:11' ,
    }
  ]

} , {
  'title' : 'Node.js最新技术栈之Promise篇' ,
  'topicType' : '分享' ,
  'topicTypeEn' : 'share' ,
  'authorId' : '5695af201a447ae81b65975b' ,
  'authorName' : 'Mr_right' ,
  'authorAvator' : './assets/avators/32dbf5ad-cef2-425f-a790-9463f6bbe70c.png' ,
  'createTime' : '2016-01-12 23:48' ,
  'content' : '通过nodejs发短信，想想就很激动，这样我就可以给网站加个会员注册短信验证功能、会员登录短信验证功能、网站异常自定义发短信功能。下面我将介绍如何通过阿里大鱼提供的api接                     口完成这些功能。' ,
  'comments' : [
    {
      'content' : '很棒，已mark' ,
      'commenterId' : '56933f762125dff800ed0f0c' ,
      'commenterName' : 'wjhuang' ,
      'commenterAvator' : './assets/avators/default.png' ,
      'createTime' : '2016-01-13 15:22' ,
    }
  ]

} , {
  'title' : 'web全栈工程师常备网址导航项目' ,
  'topicType' : '精华' ,
  'topicTypeEn' : 'cream' ,
  'authorId' : '56933f762125dff800ed0f0c' ,
  'authorName' : 'wjhuang' ,
  'authorAvator' : './assets/avators/default.png' ,
  'createTime' : '2016-01-10 12:33' ,
  'content' : '通过nodejs发短信，想想就很激动，这样我就可以给网站加个会员注册短信验证功能、会员登录短信验证功能、网站异常自定义发短信功能。下面我将介绍如何通过阿里大鱼提供的api接                     口完成这些功能。' ,
  'comments' : [
    {
      'content' : '很棒，已mark' ,
      'commenterId' : '56933f762125dff800ed0f0c' ,
      'commenterName' : 'wjhuang' ,
      'commenterAvator' : './assets/avators/default.png' ,
      'createTime' : '2016-01-12 20:39' ,
    }
  ]

} , {
  'title' : 'nodejs 有md5withrsa 的加密方法吗?' ,
  'topicType' : '普通' ,
  'topicTypeEn' : 'common' ,
  'authorId' : '56933f762125dff800ed0f0c' ,
  'authorName' : 'wjhuang' ,
  'authorAvator' : './assets/avators/default.png' ,
  'createTime' : '2016-01-13 18:02' ,
  'content' : '通过nodejs发短信，想想就很激动，这样我就可以给网站加个会员注册短信验证功能、会员登录短信验证功能、网站异常自定义发短信功能。下面我将介绍如何通过阿里大鱼提供的api接                     口完成这些功能。' ,
  'comments' : [
    {
      'content' : '很棒，已mark' ,
      'commenterId' : '56933f762125dff800ed0f0c' ,
      'commenterName' : 'wjhuang' ,
      'commenterAvator' : './assets/avators/default.png' ,
      'createTime' : '2016-01-13 18:36' ,
    }
  ]

} , {
  'title' : '【新手】node+express 渲染页面问题，求助' ,
  'topicType' : '普通' ,
  'topicTypeEn' : 'common' ,
  'authorId' : '5694db9f45ab721c18eb9143' ,
  'authorName' : 'bigTop' ,
  'authorAvator' : './assets/avators/4da4ce3c-5f69-4ca7-9df7-b31efa9a6aa3.png' ,
  'createTime' : '2016-01-12 21:27' ,
  'content' : '通过nodejs发短信，想想就很激动，这样我就可以给网站加个会员注册短信验证功能、会员登录短信验证功能、网站异常自定义发短信功能。下面我将介绍如何通过阿里大鱼提供的api接                     口完成这些功能。' ,
  'comments' : [
    {
      'content' : '很棒，已mark' ,
      'commenterId' : '56933f762125dff800ed0f0c' ,
      'commenterName' : 'wjhuang' ,
      'commenterAvator' : './assets/avators/default.png' ,
      'createTime' : '2016-01-13 08:56' ,
    }
  ]

} ];
/*topicList.forEach (function ( item ) {
  topicModel.create ( item , function ( err , data ) {
    if ( err ) {
      throw err;
    } else {
      console.log ( '主题添加成功' );
    }
  } )

} )*/
userModel.find({},function(err,users){
    users.forEach(function(user){
        user.score=0;
        user.sign="";
        user.save(function(err){
          console.log('success');
        })
    })
})
